provider "aws" {
  region = var.home-region
}

data "aws_vpc" "main" {
  tags = {
    Name = "Main VPC"
  }

}

data "aws_subnets" "public" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.main.id]
  }

  tags = {
    Public = true
  }
}

data "aws_security_group" "web" {
  tags = {
    Name = "web"
  }
}

resource "aws_lb" "main" {
  security_groups            = [data.aws_security_group.web.id]
  subnets                    = [for subnet_id in data.aws_subnets.public.ids : subnet_id]
  enable_deletion_protection = false

  tags = {
    Name = "main"
  }
}

resource "aws_lb_target_group" "web_servers" {
  name_prefix = "tg-"
  port        = 80
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = data.aws_vpc.main.id

  health_check {
    path                = "/"
    port                = "traffic-port"
    protocol            = "HTTP"
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    interval            = 30
  }
}

resource "aws_lb_listener" "web_servers" {
  load_balancer_arn = aws_lb.main.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.web_servers.arn
  }
}

resource "aws_launch_template" "web_server" {
  name          = "web_server"
  image_id      = "ami-087c4d241dd19276d" #Amazon Linux 2023
  instance_type = "t3.micro"

  network_interfaces {
    security_groups             = [data.aws_security_group.web.id]
    associate_public_ip_address = true
  }

  user_data = filebase64("scripts/web_server_init.sh")

  iam_instance_profile {
    name = aws_iam_instance_profile.web_server.name
  }

  tags = {
    Name = "Web server"
  }
}

resource "aws_autoscaling_group" "web_servers" {
  name                 = "web_servers"
  desired_capacity     = 2
  max_size             = 3
  min_size             = 1
  health_check_type    = "EC2"
  termination_policies = ["OldestInstance"]
  vpc_zone_identifier  = [for subnet in data.aws_subnets.public.ids : subnet]

  launch_template {
    id      = aws_launch_template.web_server.id
    version = "$Latest"
  }
}

resource "aws_autoscaling_schedule" "add" {
  scheduled_action_name  = "add"
  desired_capacity       = 3
  max_size               = 3
  min_size               = 1
  autoscaling_group_name = aws_autoscaling_group.web_servers.name
  recurrence             = "0 8 * * *"
}

resource "aws_autoscaling_schedule" "terminate" {
  scheduled_action_name  = "terminate"
  desired_capacity       = 1
  max_size               = 3
  min_size               = 1
  autoscaling_group_name = aws_autoscaling_group.web_servers.name
  recurrence             = "0 18 * * *"
}

resource "aws_autoscaling_attachment" "web_servers" {
  autoscaling_group_name = aws_autoscaling_group.web_servers.id
  lb_target_group_arn    = aws_lb_target_group.web_servers.arn
}

resource "aws_iam_instance_profile" "web_server" {
  name = "web_server"
  role = aws_iam_role.secret.name
}

data "aws_iam_policy_document" "assume_role" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role" "secret" {
  name               = "SecretsManagerReadWrite"
  path               = "/"
  assume_role_policy = data.aws_iam_policy_document.assume_role.json
}

resource "aws_iam_policy_attachment" "secret" {
  name       = "SecretsManagerReadWrite"
  roles      = [aws_iam_role.secret.name]
  policy_arn = "arn:aws:iam::aws:policy/SecretsManagerReadWrite"
}