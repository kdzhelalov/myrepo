#!/bin/bash

sudo yum install nginx -y
sudo systemctl start nginx
export SECRET=$(aws secretsmanager get-secret-value --secret-id=my-secret --query SecretString --output text)
echo "<h1>Host $(curl ifconfig.me), secret = $SECRET</h1>" > /usr/share/nginx/html/index.html