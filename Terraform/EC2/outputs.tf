output "server_ip" {
  value = aws_instance.web_server.public_ip
}

output "caller_username" {
  value = basename(data.aws_caller_identity.current.arn)
}