provider "aws" {
  region = var.region
}

data "aws_caller_identity" "current" {}

data "aws_subnet" "public1" {
  tags = {
    Name = "Public Subnet 1"
  }
}

data "aws_security_group" "web" {
  tags = {
    Name = "web"
  }
}

resource "aws_instance" "web_server" {
  ami                         = "ami-087c4d241dd19276d" #Amazon Linux 2023
  instance_type               = "t3.micro"
  vpc_security_group_ids      = [data.aws_security_group.web.id]
  subnet_id                   = data.aws_subnet.public1.id
  associate_public_ip_address = true

  user_data = <<-EOF
    #!/bin/bash
    sudo yum install docker -y
    systemctl start docker
    sudo docker run -d -p 80:80 nginx
    docker exec $(docker ps -q) /bin/bash -c 'echo "<h1>Hello, world!</h1>" > /usr/share/nginx/html/index.html'
    EOF

  tags = {
    Name = "web-server"
  }
}

resource "random_password" "secret_value" {
  length           = 16
  special          = true
  override_special = "!@#$%^&*()_+[]{}|"
}

resource "aws_secretsmanager_secret" "password" {
  name = "password"
}

resource "aws_secretsmanager_secret_version" "password" {
  secret_id     = aws_secretsmanager_secret.password.id
  secret_string = random_password.secret_value.result
}

