provider "aws" {
  region = var.region
}

data "aws_vpc" "main" {
  tags = {
    Name = "Main VPC"
  }
}

data "aws_subnets" "public" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.main.id]
  }

  tags = {
    Public = true
  }
}

data "aws_security_group" "web" {
  tags = {
    Name = "web"
  }
}

resource "aws_instance" "test" {
  count                       = 2
  ami                         = "ami-087c4d241dd19276d" #Amazon Linux 2023
  instance_type               = "t3.micro"
  vpc_security_group_ids      = [data.aws_security_group.web.id]
  subnet_id                   = data.aws_subnets.public.ids[count.index]
  associate_public_ip_address = true

  user_data = <<-EOF
    #!/bin/bash
    sudo yum install nginx -y
    sudo systemctl start nginx
    echo "<h1>Host${count.index}" > /usr/share/nginx/html/index.html 
    EOF

  tags = {
    Name = "TestHost-${count.index}"
  }
}

resource "aws_elb" "main" {
  name            = "main"
  subnets         = [for subnet_id in data.aws_subnets.public.ids : subnet_id]
  security_groups = [data.aws_security_group.web.id]

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:80/"
    interval            = 30
  }

  instances                   = [for i in aws_instance.test : i.id]
  cross_zone_load_balancing   = false
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400

  tags = {
    Name = "main"
  }
}