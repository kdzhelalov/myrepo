#!/bin/bash
# Description: Script for replicating data to remote host using rsync with retry logic.

# Redirecting both standard output and standard error to a log file
exec &>> /var/log/replication.log

# Define source and destination directories, and SSH key path
source_directory="/mnt/data-hdd/records"
destination_directory="/mnt/data-hdd/records"
ssh_key="/home/rsync/.ssh/id_rsa"

# Function to perform rsync with retry logic
run_rsync() {
  local host="$1"

  # Iterate over folders within the source_directory
  for folder in "$source_directory"/*; do
    if [ -d "$folder" ]; then
      folder_name=$(basename "$folder")

      echo "Processing folder: $folder_name"

      # Get the list of the latest 5 subfolders
      subfolders=$(ls "$folder" | tail -n 5)

      # Iterate over the list of subfolders
      for subfolder in $subfolders; do
        count=1
        max_count=3

        while [ "$count" -le "$max_count" ]; do
          echo "Processing subfolder: $subfolder (Attempt $count)"

          # Execute rsync command to copy subfolder to the remote host
          rsync -avz --progress -e "ssh -i $ssh_key" "$folder/$subfolder/" "$host:$destination_directory/$folder_name/$subfolder/"

          exit_code=$?

          if [ $exit_code -eq 0 ]; then
            echo "SUCCESS: Subfolder $subfolder copied."
            break
          else
            echo "FAILED: Subfolder $subfolder copy attempt $count."
            ((count++))
          fi
        done
      done
    fi
  done
}

# Remote host configuration
remote_host1="rsync@192.168.0.2"
echo "Synchronizing with host: $remote_host1"
run_rsync "$remote_host1"

remote_host2="rsync@192.168.0.3"
echo "Synchronizing with host: $remote_host2"
run_rsync "$remote_host2"
