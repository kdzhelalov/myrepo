#!/bin/bash
# Description: Script to find all files which contain a desired phone number within specific directories.

exec &>> records.log

number="0501111111"
target_dir="/mnt/data-hdd/records/cellular"
result_dir="/mnt/data-hdd/$number"
start_dir="D_2023-08-15"
end_dir="D_2023-10-23"

# Create the result directory for storing selected records
mkdir -p "$result_dir"
touch "$result_dir/records_list.txt"

# Iterate over directories in the target directory
for dir in "$target_dir"/*; do
    # Extract the dir name.
    dir_name=$(basename "$dir")

    # Check if the current item is a dir and within the specified dir range
    if [ -d "$dir" ] && [[ "$dir_name" > "$start_dir" && "$dir_name" < "$end_dir" ]]; then
        # Find records with the specified number and append paths to records_list.txt
        find "${target_dir}/${dir_name}" -name "*${number}*" >> "$result_dir/records_list.txt"
    fi
done

# Read the records list from records_list.txt
records_list=$(cat "$result_dir/records_list.txt")

# Iterate over the records list, create folders, and copy records
for record_path in $records_list; do
    folder=$(echo "$record_path" | awk -F"/" '{print $6"/"$7}')
    mkdir -p "$result_dir/$folder"
    cp "$record_path" "$result_dir/$folder"
done
