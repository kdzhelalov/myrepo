#!/bin/bash
# Description: Script for checking availability of phone numbers from the list.

# Iterate through each phone number in numbers.csv
for NUM in $(cat numbers.csv); do
  # Originate a call for the current phone number using Asterisk
  asterisk -rx "channel originate PJSIP/$NUM@101 extension"

  # Check the current status of the channel for Down or Ringing
  asterisk -rx "core show channels concise" | grep -E '^PJSIP/101.*\b(Down|Ringing)\b'

  # Capture the exit status of the previous command
  STATUS=$?

  # Get the unique identifier (CHANNELID) of the call
  CHANNELID=$(asterisk -rx "core show channels concise" | grep -o 'PJSIP/101-.\{8\}')

  # Loop until the call status changes
  while [ $STATUS -eq 0 ]; do
    # Check for channels still Down or Ringing
    asterisk -rx "core show channels concise" | grep -E 'Down|Ringing'
    STATUS=$?

    # Check for channels that are Up
    asterisk -rx "core show channels concise" | grep 'Up'
    UP=$?

    # If the channel is Up, log the phone number and request hang-up
    if [ $UP -eq 0 ]; then
      echo "$NUM" >> res.txt
      asterisk -rx "channel request hangup $CHANNELID"
    fi

    # Sleep for 0.5 seconds before re-evaluating the loop condition
    sleep 0.5
  done
done
